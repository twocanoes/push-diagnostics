//
//  TCSOverlayView.m
//  Push Diagnostics
//
//  Created by Tim Perfitt on 8/15/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#import "TCSOverlayView.h"

@implementation TCSOverlayView

//-(id)initWithFrame:(NSRect)frameRect{
// 
//    self=[super initWithFrame:frameRect];
//    
//    if (self) {
//        
//
//        
//    }
//    return self;
//}


- (void)drawRect:(NSRect)dirtyRect{

    [[NSColor colorWithCalibratedRed:0.2 green:0.2 blue:0.2 alpha:0.95] set];
    [NSBezierPath fillRect:dirtyRect];
    
    
}
-(void)clearLog{
    self.textView.string=@"";
    
}
-(void)addLogMessage:(NSString *)inMessage {
    
    NSString *mesg=[inMessage stringByAppendingString:@"\n"];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableAttributedString* attr = [[NSMutableAttributedString alloc] initWithString:mesg];
        [attr addAttribute:NSForegroundColorAttributeName value:[NSColor whiteColor] range:NSMakeRange(0, [attr length])];
        [[self.textView textStorage] appendAttributedString:attr];
        [self.textView scrollRangeToVisible:NSMakeRange([[self.textView string] length], 0)];
    });

    
    
    NSLog(@"%@",inMessage);
}
@end
