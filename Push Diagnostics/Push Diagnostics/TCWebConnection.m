//
//  TCWebConnection.m
//  Locamotion
//
//  Created by Tim Perfitt on 9/15/12.
//  Copyright (c) 2012 Twocanoes Software, Inc. All rights reserved.
//

#import "TCWebConnection.h"
#import "NSData+Base64.h"
#import <CommonCrypto/CommonCrypto.h>
#import "NSDate+RFC1123.h"
#if TARGET_OS_IPHONE == 1
#import "TCLocamotionAppDelegate.h"
#else
#import "AppDelegate.h"
#endif
NSString * const TCWebConnectionNotification = @"TCWebConnectionNotification";

@implementation TCWebConnection

+(id)webConnection{
    NSString *webURL=@"https://api.geohopper.com";

    return [[TCWebConnection alloc] initWithHostname:[NSURL URLWithString:webURL]];

    
}
-(id)initWithHostname:(NSURL *)inURL{
  
    if (self=[super init]) {
        
        self.url=inURL;
//        self.delegate=delegate;
    
    }
    return self;
    
}

-(NSString *)signatureForData:(NSData *)data withKey:(NSString *)key{
    
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data bytes];
    
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, [data length], cHMAC);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC
                                          length:sizeof(cHMAC)];
    
    NSString *hash = [HMAC base64EncodedString];
    return hash;
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
#if TARGET_OS_IPHONE == 1
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
#endif
    

    NSLog(@"connection error! %@\n%@",[error localizedDescription], error);
//    [self.delegate connectionError:[error localizedDescription] fromConnection:self];
    ErrorBlock(self);


}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    


    [self.webData appendData:data];
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
//    NSLog(@"didReceiveResponse %@",[response description]);
    lastHTTPCode = (int)[httpResponse statusCode];

}
- (void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{
    
}
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse{
    return cachedResponse;
    
}
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)redirectResponse{
    return request;
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSString *responseString=[[NSString alloc] initWithData:self.webData encoding:NSUTF8StringEncoding];
#if TARGET_OS_IPHONE == 1

    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
#endif
    CompletedBlock(self,responseString,self.webData);

}


-(int)httpCode{
    return lastHTTPCode;
}



- (BOOL)sendRequestToPath:(NSString *)inPath type:(NSString *)inType payload:(NSDictionary *)inPayload useAPIAuth:(BOOL)inAPIAuth onCompletion:(void (^)(TCWebConnection *wc,NSString *responseString,NSData *responseData))inCompletedBlock onError:(void (^)(TCWebConnection *wc))inErrorBlock { // takes an id, returns an id
#if TARGET_OS_IPHONE == 1

    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
#endif
    CompletedBlock=inCompletedBlock;
    ErrorBlock=inErrorBlock;
    NSMutableData *signatureData=[NSMutableData data];
    
    NSDate *today=[NSDate date];
    NSString *dateHeader=[today rfc1123String];
    NSData *dateHeaderData=[dateHeader dataUsingEncoding:NSUTF8StringEncoding];
    [signatureData appendData:dateHeaderData];
    
    NSData *typeData=[[inType lowercaseString] dataUsingEncoding:NSUTF8StringEncoding];
    
    [signatureData appendData:typeData];
    
    NSData *urlData=[[inPath lowercaseString] dataUsingEncoding:NSUTF8StringEncoding];
    [signatureData appendData:urlData];
    
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSString *guid=[ud valueForKey:@"userGUID"];
        
    self.webData=[NSMutableData data];
    
    NSError *error;
    NSString *httpType=[inType uppercaseString];
    NSString *urlString=[[self.url absoluteString] stringByAppendingString:inPath];

    NSURL *requestURL=[NSURL URLWithString:urlString];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    
    BOOL isValid=NO;
    if (([httpType isEqualToString:@"POST"]) ||
        ([httpType isEqualToString:@"GET"]) ||
        ([httpType isEqualToString:@"DELETE"]) ||
        ([httpType isEqualToString:@"PUT"])
        ){
        isValid=YES;
        
    }
    
    if (isValid==NO) {
        NSLog(@"no valid command!");
        return NO;
        
    }
    [request setHTTPMethod:httpType];
    if (inPayload) {
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inPayload options:NSJSONWritingPrettyPrinted error:&error];
        
        [signatureData appendData:jsonData];
        
        NSString *resultAsString = [jsonData base64EncodedString];
        
        //[[NSString alloc] initWithData:[jsonData base64EncodedString] encoding:NSUTF8StringEncoding];
        NSData *requestData = [NSData dataWithBytes:[resultAsString UTF8String] length:strlen([resultAsString UTF8String])];
        
        
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%d", (int)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        
    }
    
    if (inAPIAuth==YES) {
#if TARGET_OS_IPHONE == 1

        NSString *auth=[self signatureForData:signatureData withKey:[(TCLocamotionAppDelegate *)[[UIApplication sharedApplication] delegate ]currentPassword]];
#else
//        NSString *auth=[self signatureForData:signatureData withKey:[(AppDelegate *)[[NSApplication sharedApplication] delegate ] currentPassword]];
        NSString *auth=nil;
        
#endif
        
        if (auth==nil) return NO;
        
        
        NSString *authWithKey=[NSString stringWithFormat:@"TCAPI:%@:%@",guid,auth];
        
        [request setValue:authWithKey forHTTPHeaderField:@"Authorization"];
        [request setValue:dateHeader forHTTPHeaderField:@"Date"];

    }
    urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [urlConnection start];
    return YES;


}

@end
