//
//  main.m
//  Push Diagnostics
//
//  Created by Tom Nook on 6/8/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSTestManager.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return TSCApplicationMain(argc, argv);
    }
}

