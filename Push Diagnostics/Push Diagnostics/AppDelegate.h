//
//  AppDelegate.h
//  Push Diagnostics
//
//  Created by Tom Nook on 6/8/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSOverlayView.h"
#import <HockeySDK/HockeySDK.h>
#import "TCSOvalButton.h"
@interface AppDelegate : NSObject <NSApplicationDelegate,BITCrashReportManagerDelegate>
@property (weak) IBOutlet TCSOvalButton *mainButton;

@property (assign,nonatomic) IBOutlet NSWindow *window;


@end
