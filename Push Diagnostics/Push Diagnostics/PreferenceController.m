//
//  PreferenceController.m
//  Push Diagnostics
//
//  Created by Tim Perfitt on 2/16/12.
//  Copyright (c) 2012 Twocanoes All rights reserved.
//

#import "PreferenceController.h"



@implementation PreferenceController

- (id)init
{
    self = [super initWithWindowNibName:@"PreferenceController"];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}


@end
