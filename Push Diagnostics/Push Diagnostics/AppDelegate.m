//
//  AppDelegate.m
//  Push Diagnostics
//
//  Created by Tom Nook on 6/8/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#define kNotificationPurchaseComplete @"TCSPurchaseComplete"
#define kProductID @"com.twocanoes.inapp.pushdiagnostics"
#define TRIALDAYS 0
#define LOGFILE [@"~/Library/Logs/pushdiags.log" stringByExpandingTildeInPath]

#import "AppDelegate.h"
#import "TCSPushOperation.h"
#import "PreferenceController.h"
#import "TCWebConnection.h"
#import "NSData+Hex.h"
#import <TCSPromo/TCSPromo.h>
#import <TCSStore/TCSStore.h>
#import "TCSLogFileWindowController.h"
#define kOverlayhangover 20
NSString *const kCourierGreen = @"CourierGreen";
NSString *const kCourierYellow = @"CourierYellow";
NSString *const kCourierRed = @"CourierRed";

NSString *const kFeedbackGreen = @"FeedbackGreen";
NSString *const kFeedbackYellow = @"FeedbackYellow";
NSString *const kFeedbackRed = @"FeedbackRed";

NSString *const kPushGreen = @"PushGreen";
NSString *const kPushYellow = @"PushYellow";
NSString *const kPushRed = @"PushRed";

NSString *const kGatewayGreen = @"GatewayGreen";
NSString *const kGatewayYellow = @"GatewayYellow";
NSString *const kGatewayRed = @"GatewayRed";



@interface AppDelegate ()

@property NSOperationQueue *queue;
@property NSDictionary *hostList;

@property PreferenceController *preferenceController;

@property NSUInteger resultsCount;
@property NSUInteger failCount;
@property NSUInteger courierCount;

@property NSString *statusText;
@property NSColor *statusTextColor;
@property NSString *courierTip;
@property NSString *gatewayTip;
@property NSString *feedbackTip;
@property NSString *cloudTip;
@property BOOL isRunning;

@property (strong,nonatomic) NSString *pushToken;

@property (assign,nonatomic) BOOL isOverlayShow;
@property (assign,nonatomic) BOOL isDoingAPNSFinalCheck;
@property (assign,nonatomic) BOOL didReceivePush;
@property (strong,nonatomic) NSMutableDictionary *hosts;

@property (weak) IBOutlet NSProgressIndicator *progressBar;

@property (weak) IBOutlet NSImageView *gatewayImage;
@property (weak) IBOutlet NSImageView *feedbackImage;
@property (weak) IBOutlet NSImageView *courierImage;
@property (weak) IBOutlet NSImageView *cloudImage;
@property (weak) IBOutlet NSImageView *altCourierImage;

@property (weak,nonatomic) IBOutlet TCSLogFileWindowController *logWindowController;
@property (weak,nonatomic) IBOutlet TCSOverlayView *overlayView;
@property (weak,nonatomic) IBOutlet NSView *mainView;
@property (weak,nonatomic) IBOutlet NSArrayController *hostArrayController;

@property (weak) IBOutlet NSButton *chevronButton;
@property (weak) IBOutlet NSPopover *courierPopover;
@property (weak) IBOutlet NSPopover *altCourierPopover;

@property (weak) IBOutlet NSPopover *gatewayPopover;
@property (weak) IBOutlet NSPopover *feedbackPopover;
@property (weak) IBOutlet NSPopover *cloudPopover;


@end

@implementation AppDelegate


#pragma mark - Class Methods

+ (void)initialize
{
    // Register the preference defaults early.
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"networkTimeout" : @30, @"sandboxHosts" : @YES}];


}
-(void)readHostsFromFile{

    if (!self.hosts){

        self.hosts=[NSMutableDictionary dictionary];
    }
    [self.hosts removeAllObjects];
    NSError *err;
    NSString *csvPath=[[NSBundle mainBundle] pathForResource:@"appleports" ofType:@"txt"];

    NSString *csvString=[NSString stringWithContentsOfFile:csvPath encoding:NSUTF8StringEncoding error:&err];

    NSArray *lines=[csvString componentsSeparatedByString:@"\n"];

    [lines enumerateObjectsUsingBlock:^(NSString *currLine, NSUInteger idx, BOOL * _Nonnull stop) {
        NSArray *items=[currLine componentsSeparatedByString:@"\t"];

        if (currLine.length==0) return;
        NSArray *ports=[items[2] componentsSeparatedByString:@","];
        [ports enumerateObjectsUsingBlock:^(NSString *currPort, NSUInteger idx, BOOL * _Nonnull stop) {
            NSUUID *key=[NSUUID UUID];
            NSMutableDictionary *host=[@{@"category":items[0],
                                 @"host":items[1],
                                 @"port":currPort,
                                 @"proto":items[3],
                                 @"platform":items[4],
                                 @"note":items[5],
                                 @"status":@"Untested",
                                 @"uuid":key
            } mutableCopy];
            [self.hosts setObject:host forKey:key];
        }];
    }];
}
- (IBAction)buyButtonPressed:(id)sender {
    [self buy:self];

}
-(void)buy:(id)sender{
    [[TCSStoreManager sharedManager] setMainWindow:self.window];

    [[TCSStoreManager sharedManager] setMoreInfoLink:@"https://twocanoes.com/mac/in-app/push-diagnostics-purchasing-help"];

    [[TCSStoreManager sharedManager] setProductIdentifer:kProductID];
    [[TCSStoreManager sharedManager] setTrialDays:TRIALDAYS];
    [[TCSStoreManager sharedManager] showTrialBlocker:self];
}


- (IBAction)moreAppsMenuItemSelected:(id)sender {
//    [[TCSPromoManager sharedPromoManager] showPromoWindow:self ];
}

- (void) showMainApplicationWindow
{
    // launch the main app window
    [self.window makeFirstResponder: nil];
    [self.window makeKeyAndOrderFront: nil];
}
#pragma mark - NSApp Methods
-(void)showPromos:(id)sender{
//    [[TCSPromoManager sharedPromoManager] showPromoWindow:self];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationPurchaseComplete object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        NSLog(@"Purchase completed");
    }];
    NSString *logFile=LOGFILE;
    NSFileManager *fm=[NSFileManager defaultManager];

    [self readHostsFromFile];
    [self.hostArrayController addObjects:[self.hosts allValues]];
    if ([fm fileExistsAtPath:logFile]==NO){

        NSError *err;
        if([fm createDirectoryAtPath:[logFile stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:&err]==NO){
            [[NSAlert alertWithError:err] runModal];
        }
        if([@"Starting Up...\n" writeToFile:logFile atomically:NO encoding:NSUTF8StringEncoding error:&err]==NO){

            [[NSAlert alertWithError:err] runModal];
        }


    }
    self.logWindowController.logFile=logFile;
    [self.logWindowController setup];
    if([[TCSStoreManager sharedManager] hasPurchased]==NO) {
        [self buy:self];

    }

    [[TCSStoreManager sharedManager] setNewsletterURL:@"https://twocanoes.com/subscribe/"];
    [[TCSStoreManager sharedManager] showNewsletterIfNeeded];

    self.isRunning=NO;
    NSDate *lastTimePromoShown=[[NSUserDefaults standardUserDefaults] objectForKey:@"lastTimePromoShown"] ;

    if (lastTimePromoShown==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastTimePromoShown"];
        lastTimePromoShown=[NSDate date];
    }

    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"hidePromo"] boolValue]==NO && [[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]==NO && (fabs([lastTimePromoShown timeIntervalSinceNow])>60*60*24*7) ) {

        [self performSelector:@selector(showPromos:) withObject:self afterDelay:1];

        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastTimePromoShown"];
    }

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstLaunch"];



    
    //  Create our queue
    self.isOverlayShow=NO;
    self.isDoingAPNSFinalCheck=NO;
    self.statusText = @"";
    self.mainButton.title=@"Start";
    self.statusTextColor=[NSColor systemBlueColor];
    //  Register for our test notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(anyThread_SentUpdate:)
                                                 name:TCS_TestUpdatedNotification
                                               object:nil];
    
    //  Load in the host list plist
    //  Host list from plist in bundle
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"hostList" ofType:@"plist"];
    self.hostList = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    NSRect startRect;
    self.chevronButton.image=[NSImage imageNamed:@"chevron_white_down.png"];
    startRect=self.mainView.frame;

    startRect.origin.y=startRect.size.height-kOverlayhangover;
    self.overlayView.frame=startRect;
    [self.mainView addSubview:self.overlayView];
    
    [self.courierImage addTrackingRect:self.courierImage.bounds owner:self userData:@"courier" assumeInside:YES];
    [self.feedbackImage addTrackingRect:self.feedbackImage.bounds owner:self userData:@"feedback" assumeInside:YES];
    [self.gatewayImage addTrackingRect:self.gatewayImage.bounds owner:self userData:@"gateway" assumeInside:YES];
    [self.cloudImage addTrackingRect:self.cloudImage.bounds owner:self userData:@"cloud" assumeInside:YES];

    [self resetUI];

}



- (void)application:(NSApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken
{
    self.pushToken=[devToken hexString];
    [self updateLog:[NSString stringWithFormat:@"Registered for APNs with token %@",[devToken hexString]]];
}

- (IBAction)requestPushMenuItem:(id)sender {
    [self requestPush];
}


-(void)requestPush{
    if (self.pushToken) {
        self.cloudTip = @"Sent request for push to twocanoes server.  Waiting to receive push from APNs.";

        TCWebConnection *wc=[TCWebConnection webConnection];

        [wc sendRequestToPath:[NSString stringWithFormat:@"/pushdiags/%@",self.pushToken] type:@"GET" payload:nil useAPIAuth:NO onCompletion:^(TCWebConnection *wc, NSString *responseString, NSData *responseData) {
            
            if ([wc httpCode]==200) {
                [self updateLog:@"Sent Push. Waiting for a response"];
            }
            else {
                [self updateLog:@"Error requesting push from the twocanoes servers"];
                self.isDoingAPNSFinalCheck=NO;
                [self completeMessage];

            }
            
        } onError:^(TCWebConnection *wc) {
            [self updateLog:@"Error requesting push from the twocanoes servers"];
            self.isDoingAPNSFinalCheck=NO;
            [self completeMessage];

        }];
    }
    else {
        NSLog(@"No Push Token, so skipping push test");
        [self updateLog:@"No Push Token so can't request push"];
        self.cloudTip=@"No Push Token, so skipping push test";
        self.isDoingAPNSFinalCheck=NO;
        [self completeMessage];


        
    }

}

- (void)application:(NSApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    [self updateLog:@"Could not register with APNs"];

}

- (void)application:(NSApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [self updateLog:@"Received Push Notification"];
    self.didReceivePush=YES;
    self.cloudTip = @"Received Push Notification.";

    [self completeMessage];


}




- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
	return YES;
}

#pragma mark - IBActions

// Open the preferences

- (IBAction)showPreferences:(id)sender {
    
    // If there isn't a preferenceController make one.
    if (!self.preferenceController) {
        self.preferenceController = [PreferenceController new];
    }
    
    [self.preferenceController showWindow:self];
}



- (void)resetUI {
    //  Set the images
    [self.courierImage setImage:[NSImage imageNamed:@"CourierGray"]];
    [self.gatewayImage setImage:[NSImage imageNamed:@"GatewayGray"]];
    [self.feedbackImage setImage:[NSImage imageNamed:@"FeedbackGray"]];
    [self.cloudImage setImage:[NSImage imageNamed:@"PushGray"]];

    
    //  Clear the tips
    self.courierTip = @"The courier server. Run tests to see more infomation.";
    self.feedbackTip = @"The feedback server. Run tests to see more infomation.";
    self.gatewayTip = @"The gateway server. Run tests to see more infomation.";
    self.cloudTip = @"The push network. Run tests to see more infomation.";

    
    //  Reset the status text
    self.statusText = @"";
    self.mainButton.title=@"Start";
}

- (void)toggleOverlay
{
    NSRect startRect,endRect;

    if (self.isOverlayShow==NO) {
        
        startRect=self.mainView.frame;

        startRect.origin.y=startRect.size.height-kOverlayhangover;
        self.overlayView.frame=startRect;
        endRect=self.mainView.frame;
        self.chevronButton.image=[NSImage imageNamed:@"chevron_white"];

    }
    else {
        
        startRect=self.mainView.frame;

        endRect=self.mainView.frame;
        
        endRect.origin.y=startRect.size.height-kOverlayhangover;
        self.overlayView.frame=startRect;
        self.chevronButton.image=[NSImage imageNamed:@"chevron_white_down"];

    }
    
        



    // firstView, secondView are outlets
    NSViewAnimation *theAnim;
    NSMutableDictionary* firstViewDict;
    

    // Create the attributes dictionary for the first view.
    firstViewDict = [NSMutableDictionary dictionaryWithCapacity:3];
    
    // Specify which view to modify.
    [firstViewDict setObject:self.overlayView forKey:NSViewAnimationTargetKey];
    
    // Specify the starting position of the view.
    [firstViewDict setObject:[NSValue valueWithRect:startRect]
                      forKey:NSViewAnimationStartFrameKey];
    
    // Change the ending position of the view.

    [firstViewDict setObject:[NSValue valueWithRect:endRect]
                      forKey:NSViewAnimationEndFrameKey];


    
    // Create the view animation object.
    theAnim = [[NSViewAnimation alloc] initWithViewAnimations:[NSArray
                                                               arrayWithObjects:firstViewDict, nil]];
    
    // Set some additional attributes for the animation.
    [theAnim setDuration:0.25];    // One and a half seconds.
    [theAnim setAnimationCurve:NSAnimationEaseInOut];
    
    // Run the animation.
    [theAnim startAnimation];
    self.isOverlayShow=!self.isOverlayShow;

}
-(void)cancelTasks{
    //  Clear the queue out before we add anything
    [self.queue cancelAllOperations];
    
    //  This is a terrible terrible hack. TODO: Figure out how to clear the queue for reuse.
    self.queue = nil;
    
    //  Update the status text and log
    self.statusText = @"";
    self.mainButton.title=@"Start";
    [self updateLog:@"APNs tests canceled by user #info #network"];
    [self.progressBar setIndeterminate:NO];
    [self.progressBar setHidden:YES];

    
}

- (IBAction)textClick:(id)sender {

    //  Are we already running?
    if ([[self.queue operations] count] > 0) {
        [self cancelTasks];
        self.isRunning=NO;
    } else if (self.isDoingAPNSFinalCheck==YES) {
        self.isDoingAPNSFinalCheck=NO;
        self.cloudTip=@"No push received.";
        [self cancelTasks];
        self.isRunning=NO;
        return;
    } else {
        self.isRunning=YES;
        //  Reset the test counters
        self.resultsCount = 0;
        self.failCount = 0;
        self.courierCount = 0;
        
        //  Determine sandbox and make test arrays
        NSDictionary *testList;
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"sandboxHosts"]) {
            testList = self.hostList[@"hostListSandbox"];
        } else {
            testList = self.hostList[@"hostListProd"];
        }




        //  Reset the globes and tips
        [self resetUI];
        self.mainButton.title=@"Cancel";

        //  Create a queue - TODO: Figure out how to reuse this.
        self.queue = [[NSOperationQueue alloc] init];
        
        //  Clear the queue out before we add anything
        [self.queue cancelAllOperations];
        
        //  Update the status text and log
        self.statusText = @"Tests are running";
        [self updateLog:@"APNs tests beginning #info #network"];

        //  Make our tests
        [[self.hosts allValues] enumerateObjectsUsingBlock:^(NSDictionary *currHost, NSUInteger idx, BOOL * _Nonnull stop) {
            TCSPushOperation *keyTest = [[TCSPushOperation alloc] initWithTestFor:currHost named:[currHost objectForKey:@"host"]];

            //  Start our test
            [self.queue addOperation:keyTest];

        }];

        
    }
}

#pragma mark - UI Update Methods


//  Receive update notifications from the queues and send them to the main thread
- (void)anyThread_SentUpdate:(NSNotification *)note {
    
    // update our UI on the main thread
	[self performSelectorOnMainThread:@selector(updateUI:) withObject:note waitUntilDone:YES];
    
}

//  Format the log and tip message
- (NSString *)formatLogTextFor:(NSDictionary *)testInfo {
    switch ([testInfo[kTestResultsKey] integerValue]) {
        case kTCSPushTestPass:
            return [NSString stringWithFormat: @"Connected to %@ (%@) at IP address %@ on port %@", [testInfo[kServiceLabelTextKey] capitalizedString],testInfo[kServiceNameTextKey], testInfo[kServiceIPAddressTextKey], testInfo[kServicePortTextKey]];
            break;
            
        case kTCSPushTestFail:
            return [NSString stringWithFormat: @"Connection failed for %@ (%@) IP address %@ on port %@", [testInfo[kServiceLabelTextKey] capitalizedString],testInfo[kServiceNameTextKey], testInfo[kServiceIPAddressTextKey], testInfo[kServicePortTextKey]];
            break;
            
        case kTCSPushTestProxy:
            return [NSString stringWithFormat: @"Possible proxy between client and %@ (%@)", [testInfo[kServiceLabelTextKey] capitalizedString],testInfo[kServiceNameTextKey]];
            break;
            
        case kTCSPushTestDNSFailure:
            return [NSString stringWithFormat: @"Could not resolve DNS for %@ (%@)", [testInfo[kServiceLabelTextKey] capitalizedString],testInfo[kServiceNameTextKey]];
            break;
        case kTCSPushTestTimeout:
            return [NSString stringWithFormat: @"Timeout for %@ (%@)", [testInfo[kServiceLabelTextKey] capitalizedString],testInfo[kServiceNameTextKey]];
            break;

        default:
            [self updateLog:@"Unknown error has occured! #error"];
            return nil;

            break;
            
    }
    
}

-(void)updateLog:(NSString *)inString{
    NSString *logFile=LOGFILE;


    NSFileHandle *fh=[NSFileHandle fileHandleForWritingAtPath:logFile];
    [fh seekToEndOfFile];

    [fh writeData:[[inString  stringByAppendingString:@"\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [fh closeFile];
//    [self.overlayView addLogMessage:inString];
}
//  Update the UI
- (void)updateUI:(NSNotification *)note {

    TCSPushOperation *currPushOperation=(TCSPushOperation *)note.object;
    NSDictionary *currHostInfo=[currPushOperation hostInfo];

    //  Get the userInfo dict
    NSDictionary *testInfo = [note userInfo];
    NSUUID *uuid=[currHostInfo objectForKey:@"uuid"];
    NSInteger currentIndex=[[self.hosts allKeys] indexOfObject:uuid];
    NSMutableArray *currObject=[self.hostArrayController mutableArrayValueForKey:@"arrangedObjects"];

    if (currPushOperation.ipAddress){
        currObject[currentIndex][@"ipaddress"]=currPushOperation.ipAddress;

    }
    if (currPushOperation.reverseDNSName){
        currObject[currentIndex][@"reverseDNSName"]=currPushOperation.reverseDNSName;

    }
    if (testInfo[@"message"]) {

        currObject[currentIndex][@"status"]=testInfo[@"message"];


        [self updateLog:[NSString stringWithFormat:@"%@ (%@): %@",[testInfo[kServiceLabelTextKey] capitalizedString],testInfo[kServiceNameTextKey],testInfo[@"message"]]];
        return;
        
    }
    else {
    //  We got a result back so add it to our counter.
    }
    NSString *const kServiceLabelTestType = @"kServiceLabelTestType";

    //  What updated and which Network Globe to light up?
    if (!testInfo[kTestResultsKey]) return;
    switch ([testInfo[kTestResultsKey] integerValue]) {

        case kTCSPushTestPass:

            self.resultsCount++;
            currObject[currentIndex][@"color"]=[NSColor textColor];
            if (testInfo[@"message"]) {

                currObject[currentIndex][@"status"]=testInfo[@"message"];
            }
            else {

                currObject[currentIndex][@"status"]=[NSString stringWithFormat:@"Pass (%@)",testInfo[kServiceLabelTestType]];
            }
            break;

        case kTCSPushTestFail:
            self.resultsCount++;
            self.failCount++;
            currObject[currentIndex][@"color"]=[NSColor redColor];
            if (testInfo[@"message"]) {

                currObject[currentIndex][@"status"]=testInfo[@"message"];
            }
            else {
                currObject[currentIndex][@"status"]=@"Failed";
            }
            break;

//            //  Record a failure
//            self.failCount++;
//
//            if ([testInfo[kServiceNameTextKey] hasPrefix:@"gateway"] && ([testInfo[kServicePortTextKey] isEqual: @2196])) {
//                [self.feedbackImage setImage:[NSImage imageNamed:kFeedbackRed]];
//                self.feedbackTip = [self formatLogTextFor:testInfo];
//                [self updateLog:self.feedbackTip];
//
//                break;
//
//            } else if ([testInfo[kServiceNameTextKey] hasPrefix:@"gateway"]) {
//
//                [self.gatewayImage setImage:[NSImage imageNamed:kGatewayRed]];
//                self.gatewayTip = [self formatLogTextFor:testInfo];
//                [self updateLog:self.gatewayTip];
//
//                break;
//
//            } else {
//                [self.courierImage setImage:[NSImage imageNamed:kCourierRed]];
//                self.courierTip = [self formatLogTextFor:testInfo];
//                [self updateLog:self.courierTip];
//                self.courierCount++;
//
//                break;
//            }
            
//        case kTCSPushTestProxy:
////            //  Record a failure
////            self.failCount++;
////
////            if ([testInfo[kServiceNameTextKey] hasPrefix:@"gateway"] && ([testInfo[kServicePortTextKey] isEqual: @2196])) {
////                [self.feedbackImage setImage:[NSImage imageNamed:kGatewayYellow]];
////                self.feedbackTip = [self formatLogTextFor:testInfo];
////                [self updateLog: self.feedbackTip];
////
////                break;
////
////            } else if ([testInfo[kServiceNameTextKey] hasPrefix:@"gateway"]) {
////
////                [self.gatewayImage setImage:[NSImage imageNamed:kGatewayYellow]];
////                self.gatewayTip = [self formatLogTextFor:testInfo];
////                [self updateLog:self.gatewayTip];
////
////                break;
////
////            } else {
////                [self.courierImage setImage:[NSImage imageNamed:kCourierYellow]];
////                self.courierTip = [self formatLogTextFor:testInfo];
////                [self updateLog:self.courierTip];
////                self.courierCount++;
////
////                break;
////            }
//            self.resultsCount++;
//
//            currObject[currentIndex][@"status"]=@"Proxy Failure";
//            break;

        case kTCSPushTestDNSFailure:
            //  Record a failure
//            self.failCount++;
//
//            if ([testInfo[kServiceNameTextKey] hasPrefix:@"gateway"] && ([testInfo[kServicePortTextKey] isEqual: @2196])) {
//                [self.feedbackImage setImage:[NSImage imageNamed:kFeedbackRed]];
//                self.feedbackTip = [self formatLogTextFor:testInfo];
//                [self updateLog:self.feedbackTip];
//
//                break;
//
//            } else if ([testInfo[kServiceNameTextKey] hasPrefix:@"gateway"]) {
//
//                [self.gatewayImage setImage:[NSImage imageNamed:kGatewayRed]];
//                self.gatewayTip = [self formatLogTextFor:testInfo];
//                [self updateLog:self.gatewayTip];
//
//                break;
//
//            } else { [self.courierImage setImage:[NSImage imageNamed:kCourierRed]];}
//            self.courierTip = [self formatLogTextFor:testInfo];
//            [self updateLog:self.courierTip];
//            self.courierCount++;
            currObject[currentIndex][@"status"]=@"DNS Failure";
            self.resultsCount++;
            self.failCount++;
            currObject[currentIndex][@"color"]=[NSColor redColor];

            break;
            
        case kTCSPushTestTimeout:
            currObject[currentIndex][@"status"]=@"Timeout";
            self.failCount++;
            self.resultsCount++;
            currObject[currentIndex][@"color"]=[NSColor redColor];

            break;


    }
    
    //  Are we done?
    if (self.resultsCount == [[self.hosts allKeys] count]) {

//        if (self.pushToken) {
//            self.isDoingAPNSFinalCheck=YES;
//            self.statusText=@"Sending ourselves a push notification";
//            [self.progressBar setIndeterminate:YES];
//            [self.progressBar startAnimation:self];
//            [self.overlayView addLogMessage:@"Trying to sending ourselves a push notification"];
//            self.didReceivePush=NO;
//            [self requestPush];
//
//            [self performSelector:@selector(checkUpOnPush:) withObject:nil afterDelay:[[NSUserDefaults standardUserDefaults] integerForKey:@"networkTimeout"]];
//        }
//        else {
            [self completeMessage];
//        }

        //  This is a terrible terrible hack. TODO: Figure out how to clear the queue for reuse.
        self.queue = nil;
    }
    
    
    
}
-(void)completeMessage{
//    if (self.didReceivePush==YES) {
        self.statusText = [NSString stringWithFormat:@"APNs tests completed with %lu passed and %lu failed.\n", (self.resultsCount - self.failCount), (NSUInteger)self.failCount];
        [self updateLog:[NSString stringWithFormat:@"APNs tests completed with %lu passed and %lu failed. #info #network", (self.resultsCount - self.failCount), (NSUInteger)self.failCount]];
        
//        [self.cloudImage setImage:[NSImage imageNamed:@"PushGreen"]];
//    }
//
//    else {
//        self.statusText = [NSString stringWithFormat:@"APNs tests completed with %lu passed and %lu failed.\nTest Push failed.", (self.resultsCount - self.failCount), (NSUInteger)self.failCount];
//        [self.overlayView addLogMessage:[NSString stringWithFormat:@"APNs tests completed with %lu passed and %lu failed. #info #network", (self.resultsCount - self.failCount), (NSUInteger)self.failCount]];
//
//        [self.cloudImage setImage:[NSImage imageNamed:@"PushRed"]];
//
//
//
//    }

    [self.progressBar setIndeterminate:NO];
    [self.progressBar setHidden:YES];
    self.isDoingAPNSFinalCheck=NO;
    self.mainButton.title=@"Start";
    if (self.failCount==0 ) {
        NSSound *goodSound=[NSSound soundNamed:@"finishSound"];
        [goodSound play];
    }
    else {
        NSSound *failedSound=[NSSound soundNamed:@"fail"];

        [failedSound play];
        
    }


}
-(void)checkUpOnPush:(id)sender{
    if (self.didReceivePush==NO){
        [self updateLog:@"No push received..."];
        self.isDoingAPNSFinalCheck=NO;
        self.cloudTip = @"No push received.";

        [self completeMessage];
    }

}

- (IBAction)showLogButtonPressed:(id)sender {

    [[NSWorkspace sharedWorkspace] openFile:LOGFILE];


}

@end
