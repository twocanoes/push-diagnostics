//
//  NSData+Hex.m
//  Geohopper
//
//  Created by Tim Perfitt on 11/15/12.
//  Copyright (c) 2012 Twocanoes Software, Inc. All rights reserved.
//

#import "NSData+Hex.h"

@implementation NSData (Hex)
-(NSString *)hexString
{
	NSUInteger capacity = [self length] * 2;
	NSMutableString *stringBuffer = [NSMutableString stringWithCapacity:capacity];
	const unsigned char *dataBuffer = [self bytes];
    
	for (NSUInteger i = 0; i < [self length]; ++i) {
		[stringBuffer appendFormat:@"%02X",(unsigned int)dataBuffer[i]];
	}
	if (stringBuffer) {
		return stringBuffer;
	}
	return nil;
}

@end
