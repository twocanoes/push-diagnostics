//
//  TCSOverlayView.h
//  Push Diagnostics
//
//  Created by Tim Perfitt on 8/15/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TCSOverlayView : NSView
-(void)addLogMessage:(NSString *)inMessage ;
-(void)clearLog;
@property (weak) IBOutlet NSScrollView *scrollView;

@property (unsafe_unretained) IBOutlet NSTextView *textView;
@end
