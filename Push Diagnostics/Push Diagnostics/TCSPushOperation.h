//
//  TCSPushOperation.h
//  Push Diagnostics
//
//  Created by Tom Nook on 6/12/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

/// Test results for the TCSPushOperation Class
typedef NS_ENUM(NSInteger, TCS_TestResults)
{
    kTCSPushTestPass,
    kTCSPushTestFail,
    kTCSPushTestTimeout,
    kTCSPushTestDNSFailure,
    kTCSPushTestProxy
};

// NSNotification name to tell the UI to update
extern NSString *const TCS_TestUpdatedNotification;

//  Const for the status text key
extern NSString *const kServiceLabelTextKey;
extern NSString *const kServiceNameTextKey;

extern NSString *const kServiceIPAddressTextKey;
extern NSString *const kServicePortTextKey;
extern NSString *const kTestResultsKey;

@interface TCSPushOperation : NSOperation <NSStreamDelegate>
@property (strong,nonatomic) NSDictionary *hostInfo;
@property (strong,nonatomic) NSString *ipAddress;
@property (strong,nonatomic) NSString *reverseDNSName;

- (id)initWithTestFor:(NSDictionary *)testArray named:(NSString *)inName;
@property BOOL isFinished;

@end

