//
//  NSData+Hex.h
//  Geohopper
//
//  Created by Tim Perfitt on 11/15/12.
//  Copyright (c) 2012 Twocanoes Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Hex)
-(NSString *)hexString;
@end
