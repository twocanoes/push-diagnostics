//
//  TCSTestManager.h
//  Push Diagnostics
//
//  Created by Tom Nook on 8/21/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCSTestManager : NSObject
int TSCApplicationMain(int argc, char *argv[]);
@end
