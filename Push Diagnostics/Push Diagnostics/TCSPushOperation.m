//
//  TCSPushOperation.m
//  Push Diagnostics
//
//  Created by Tom Nook on 6/12/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#import "TCSPushOperation.h"
#import <netdb.h>
#import <arpa/inet.h>

NSString *const TCS_TestUpdatedNotification = @"kAPNsTestUpdated";
NSString *const kServiceNameTextKey = @"kServiceNameTextKey";
NSString *const kServiceLabelTextKey = @"kServiceNameLabelKey";
NSString *const kServiceLabelTestType = @"kServiceLabelTestType";

NSString *const kServiceIPAddressTextKey = @"kServiceIPAddressTextKey";
NSString *const kServicePortTextKey = @"kServicePortTextKey";
NSString *const kTestResultsKey = @"kTestResultsKey";

@interface TCSPushOperation ()

//  Properties
@property (strong,nonatomic) NSString *serviceName;
@property (assign, nonatomic) UInt32 servicePort;
@property (strong,nonatomic) NSTimer *timeoutTimer;
@property (strong,nonatomic) NSTimer *cancelTimer;
@property (assign) BOOL testingProxy;
@property (strong,nonatomic) NSString *serviceLabel;

//  Stream objects
@property (strong,nonatomic) NSInputStream *inputStream;
@property (strong,nonatomic) NSOutputStream *outputStream;

@end

@implementation TCSPushOperation

//  Designated init
- (id)initWithTestFor:(NSDictionary *)hostInfo named:(NSString *)inName{
    
    self = [super init];


    if (self) {
        //  Get the values from the testArray
        self.serviceLabel=inName;
        self.serviceName = [hostInfo objectForKey:@"host"];
        self.servicePort = (UInt32)[[hostInfo objectForKey:@"port"] integerValue];
        self.hostInfo=hostInfo;
        self.testingProxy=NO;
        [self createStreamWithProxy:NO];
        return self;
    }
    
    return nil;
}
-(BOOL)systemProxyForURL:(NSURL *)url info:(NSString **)info{
    CFURLRef URL=(__bridge CFURLRef)[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",self.serviceName]];

    CFDictionaryRef systemProxySettings =CFNetworkCopySystemProxySettings();

    NSArray *proxies = CFBridgingRelease(CFNetworkCopyProxiesForURL(URL, (CFDictionaryRef) systemProxySettings)) ;


    NSDictionary * firstProxySettings =[proxies objectAtIndex:0];
    NSURL *pacScriptURL;
    if (nil != (pacScriptURL = [firstProxySettings objectForKey:(NSString *)kCFProxyAutoConfigurationURLKey]))
    {
        CFErrorRef cfErrorRef = NULL;
        NSError *nsError = nil;
        NSString *script;

        script = [NSString stringWithContentsOfURL:pacScriptURL usedEncoding:NULL error:&nsError];

        NSArray *proxies = CFBridgingRelease(CFNetworkCopyProxiesForAutoConfigurationScript((__bridge CFStringRef)script,  URL, &cfErrorRef)) ;


        firstProxySettings = [proxies objectAtIndex:0];
        NSString *proxyType=[firstProxySettings objectForKey:(NSString *)kCFProxyTypeKey];
        if (!proxyType || [proxyType isEqualToString:(NSString *)kCFProxyTypeNone] ==YES){
            *info=nil;
            return NO;
        }
        else {
            NSString *proxyHostname=[firstProxySettings objectForKey:(NSString *)kCFProxyHostNameKey];
            long proxyPort=[[firstProxySettings objectForKey:(NSString *)kCFProxyPortNumberKey] longValue];
            NSString *proxyType=[firstProxySettings objectForKey:(NSString *)kCFProxyTypeKey];

            *info=[NSString stringWithFormat:@"%@:%li (%@)",proxyHostname,proxyPort,proxyType];
            return YES;

        }
    }
    return NO;
}
-(void)createStreamWithProxy:(BOOL)useProxy{
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    NSLog(@"Creating socket connection to %@  %@",self.serviceLabel,self.serviceName);
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)(self.serviceName), self.servicePort, &readStream, &writeStream);

    self.inputStream = (__bridge_transfer NSInputStream *)readStream;
    self.outputStream = (__bridge_transfer NSOutputStream *)writeStream;
    [self.inputStream setDelegate:self];
    [self.outputStream setDelegate:self];
    [self.inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    if (useProxy==YES){
        NSDictionary    *proxySettings = CFBridgingRelease(CFNetworkCopySystemProxySettings());
        [self.inputStream  setProperty:proxySettings forKey:NSStreamSOCKSProxyConfigurationKey];
        [self.outputStream setProperty:proxySettings forKey:NSStreamSOCKSProxyConfigurationKey];

    }

    //  Start our doomsday timeout timer
    NSLog(@"Creating socket connection to %@  %@ DONE",self.serviceLabel,self.serviceName);

    self.timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:[[NSUserDefaults standardUserDefaults] integerForKey:@"networkTimeout"]
                                                         target:self
                                                       selector:@selector(timeoutReached)
                                                       userInfo:nil
                                                        repeats:NO];

    //  Start our cancel check timer.
    self.cancelTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                        target:self
                                                      selector:@selector(cancelTest)
                                                      userInfo:nil
                                                       repeats:YES];

}
- (void)main {

    //  Are we alive?
    if (!self.isCancelled) {

        [self updateUI:@{kServiceLabelTextKey:self.serviceLabel,kServiceLabelTextKey:self.serviceLabel,kServiceNameTextKey : self.serviceName,@"message":@"Resolving DNS Name"}];

        self.ipAddress = [self resolveIP];

        if (!self.ipAddress) {
            NSDictionary *updateDict = @{kServiceLabelTextKey:self.serviceLabel,kServiceLabelTextKey:self.serviceLabel,kServiceNameTextKey : self.serviceName, kServicePortTextKey : @(self.servicePort),  kTestResultsKey : @(kTCSPushTestDNSFailure)};
            [self updateUI:updateDict];
            [self cleanupAndFinish];
            
        } else if (!self.isCancelled) {
            //  Are we behind a proxy?

            [self updateUI:@{kServiceLabelTextKey:self.serviceLabel,kServiceLabelTextKey:self.serviceLabel,kServiceNameTextKey : self.serviceName,@"message":@"Checking for proxy"}];

            [self updateUI:@{kServiceLabelTextKey:self.serviceLabel,kServiceLabelTextKey:self.serviceLabel,kServiceNameTextKey : self.serviceName,@"message":@"Direct connecting..."}];
            NSLog(@"Opening a connection to %@ %@ without proxy",self.serviceLabel,self.serviceName);

            [self.inputStream open];
            [self.outputStream open];


            

        } else {

            // We've been cancelled!
            [self cleanupAndFinish];
        }
    }

} 


//  Method to send the test update notification
- (void)updateUI:(NSDictionary *)updateInfo {
    
    //  Send a notification that we updated
    [[NSNotificationCenter defaultCenter] postNotificationName:TCS_TestUpdatedNotification object:self userInfo:updateInfo];
}

//  Resolve the IP address of the service hostname. Sadly this is easier than a CFHost.
- (id)resolveIP {
    // Ask the unix subsytem to query the DNS
    struct hostent *remoteHostEnt = gethostbyname([self.serviceName UTF8String]);
    // Get address info from host entry
    if (remoteHostEnt) {

        struct in_addr *remoteInAddr = (struct in_addr *) remoteHostEnt->h_addr;
        // Convert numeric addr to ASCII string
        char str[INET_ADDRSTRLEN];
        const char *sRemoteInAddr = inet_ntop(AF_INET, remoteInAddr, str, sizeof(str));

        NSString* hostIP = @(sRemoteInAddr);

        NSHost *host=[NSHost hostWithAddress:hostIP];

        if (host.names.count>0){

            self.reverseDNSName=[host.names firstObject];
        }

        NSDictionary *updateDict = @{kServiceLabelTextKey:self.serviceLabel,kServiceNameTextKey : self.serviceName};
        
        //  Call the updater
        [self updateUI:updateDict];

        
        return hostIP;
    } else {
        [self statusUpdate:@"DNS not available"];
        return nil;
    }
    
}


-(void)cleanup{
    //  Close the streams
    [self.inputStream  setDelegate:nil];
    [self.outputStream setDelegate:nil];
    [self.inputStream  close];
    [self.outputStream close];
    [self.inputStream  removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [self.outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    self.inputStream  = nil;
    self.outputStream = nil;

    //  Lose the timers
    [self.cancelTimer invalidate];
    self.cancelTimer = nil;
    [self.timeoutTimer invalidate];
    self.timeoutTimer = nil;

}

-(void)cleanupAndFinish{
    [self cleanup];
    //  Turn myself off
    self.isFinished = YES;
    [self didChangeValueForKey:@"isFinished"];

}

-(void)statusUpdate:(NSString *)inMessage{
    NSDictionary *updateDict = @{kServiceLabelTextKey:self.serviceLabel,kServiceNameTextKey : self.serviceName,@"message":inMessage};
    
    //  Call the updater
    [self updateUI:updateDict];

}
//  We reached the end of our timeout
- (void)timeoutReached {
    //  Make our info dict
    //This is coming back empty when timing out and not sure why.
    if (!self.ipAddress) self.ipAddress=@"";
    NSDictionary *updateDict = @{kServiceLabelTextKey:self.serviceLabel,kServiceNameTextKey : self.serviceName, kServiceIPAddressTextKey : self.ipAddress, kServicePortTextKey : @(self.servicePort), kTestResultsKey : @(kTCSPushTestTimeout)};
    
    //  Call the updater
    [self updateUI:updateDict];
    
    //  All done
    [self cleanupAndFinish];
    
}

//  Have we been canceled?
- (void)cancelTest {
    if (self.isCancelled) {
        [self cleanupAndFinish];
    }
}

#pragma mark - Delegate Methods
//  The callback on stream status
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode {
    switch (eventCode) {
        case NSStreamEventOpenCompleted:{//NSStreamEventOpenCompleted
            NSDictionary *updateDict;
            if (self.testingProxy==NO){

                updateDict = @{kServiceLabelTextKey:self.serviceLabel,kServiceNameTextKey : self.serviceName, kServiceIPAddressTextKey : self.ipAddress, kServicePortTextKey : @(self.servicePort), kTestResultsKey : @(kTCSPushTestPass),kServiceLabelTestType:@"Direct Connection"};
            }
            else {
                updateDict = @{kServiceLabelTextKey:self.serviceLabel,kServiceNameTextKey : self.serviceName, kServiceIPAddressTextKey : self.ipAddress, kServicePortTextKey : @(self.servicePort), kTestResultsKey : @(kTCSPushTestPass),kServiceLabelTestType:@"Proxy Connection"};


            }
            
            //  Call the updater
            [self updateUI:updateDict];
            
            //  All done
            [self cleanupAndFinish];
        }
            break;
        case NSStreamEventErrorOccurred:{  //
            //  Make our info dict

            if (self.testingProxy==YES){

                NSString *info;
                NSDictionary *updateDict;
                if([self systemProxyForURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",self.serviceName]] info:&info]==YES) {
                    updateDict = @{kServiceLabelTextKey:self.serviceLabel,kServiceNameTextKey : self.serviceName, kServiceIPAddressTextKey : self.ipAddress, kServicePortTextKey : @(self.servicePort), kTestResultsKey : @(kTCSPushTestFail),@"message":[NSString stringWithFormat:@"Detected proxy @ %@",info]};

                }
                else {
                     updateDict = @{kServiceLabelTextKey:self.serviceLabel,kServiceNameTextKey : self.serviceName, kServiceIPAddressTextKey : self.ipAddress, kServicePortTextKey : @(self.servicePort), kTestResultsKey : @(kTCSPushTestFail)};


                }
                [self updateUI:updateDict];
                [self cleanupAndFinish];


            }
            else if (self.testingProxy==NO){
                self.testingProxy=YES;
                [self retryWithProxy];

            }
            
        }
        default:
            break;
    }
    
}
-(void)retryWithProxy{
    NSDictionary *updateDict = @{kServiceLabelTextKey:self.serviceLabel,kServiceNameTextKey : self.serviceName, kServiceIPAddressTextKey : self.ipAddress, kServicePortTextKey : @(self.servicePort), kTestResultsKey : @(kTCSPushTestProxy)};
    [self updateUI:updateDict];

    [self cleanup];
    [self createStreamWithProxy:YES];
    [self updateUI:@{kServiceLabelTextKey:self.serviceLabel,kServiceLabelTextKey:self.serviceLabel,kServiceNameTextKey : self.serviceName,@"message":@"Connecting Via Proxy"}];
    NSLog(@"Opening a connection to %@ %@ With proxy",self.serviceLabel,self.serviceName);

    [self.inputStream open];
    [self.outputStream open];


}
@end
