//
//  TCWebConnection.h
//  Locamotion
//
//  Created by Tim Perfitt on 9/15/12.
//  Copyright (c) 2012 Twocanoes Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCWebConnection.h"

@interface TCWebConnection : NSObject <NSURLConnectionDelegate>{
    NSURLConnection *urlConnection;
    int lastHTTPCode;
    void (^CompletedBlock)(TCWebConnection *wc,NSString *responseString,NSData *responseData);
    void (^ErrorBlock)(TCWebConnection *wc);

    
}
@property (retain,nonatomic) id userData;
@property (retain,nonatomic) NSMutableData *webData;
@property  NSURL *url;
+(id)webConnection;
-(id)initWithHostname:(NSURL *)inURL;
- (BOOL)sendRequestToPath:(NSString *)inPath type:(NSString *)inType payload:(NSDictionary *)inPayload useAPIAuth:(BOOL)inAPIAuth onCompletion:(void (^)(TCWebConnection *wc,NSString *responseString,NSData *responseData))inCompletedBlock onError:(void (^)(TCWebConnection *wc))inErrorBlock; // takes an id, returns an id
-(int)httpCode;

@end
