//
//  TCSLogWindowController.h
//  Winclone
//
//  Created by Timothy Perfitt on 8/16/19.
//  Copyright © 2019 Twocanoes Software, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TCSLogWindowDelegate <NSObject>

-(void)logWindowClosed;

@end
@interface TCSLogFileWindowController : NSWindowController <NSWindowDelegate>
@property (strong) NSString *logFile;
@property (assign) id<TCSLogWindowDelegate> delegate;
-(void)setup;
@end

NS_ASSUME_NONNULL_END
