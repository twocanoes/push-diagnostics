//
//  MainWindowController.h
//  Push Diagnostics
//
//  Created by Tom Nook on 6/13/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MainWindowController : NSWindowController

@end
