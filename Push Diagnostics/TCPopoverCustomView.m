//
//  TCPopoverCustomView.m
//  
//
//  Created by timothy perfitt on 8/17/13.
//
//

#import "TCPopoverCustomView.h"

@implementation TCPopoverCustomView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
	[super drawRect:dirtyRect];
    
    [[NSColor controlColor] set];
    [NSBezierPath fillRect:dirtyRect];
	
    // Drawing code here.
}

@end
