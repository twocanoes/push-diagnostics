//
//  Push_DiagnosticsTests.m
//  Push DiagnosticsTests
//
//  Created by Tom Nook on 6/8/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#import "Push_DiagnosticsTests.h"
#import "AppDelegate.h"

@interface Push_DiagnosticsTests ()

@property AppDelegate *appDelegate;

@end

@implementation Push_DiagnosticsTests

- (void)setUp
{
    [super setUp];
    _appDelegate = [[AppDelegate alloc] init];
    
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}





@end
