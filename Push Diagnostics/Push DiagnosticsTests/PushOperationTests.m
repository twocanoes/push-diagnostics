//
//  PushOperationTests.m
//  Push Diagnostics
//
//  Created by Tom Nook on 6/12/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#import "PushOperationTests.h"
#import "TCSPushOperation.h"

@interface PushOperationTests ()

@property TCSPushOperation *pushOperation;

@end

@implementation PushOperationTests

- (void)setUp
{
    [super setUp];
    _pushOperation = [[TCSPushOperation alloc] init];
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
    self.pushOperation = nil;
    
}

- (void)testCanMakeOperationObject {
    STAssertNotNil(self.pushOperation, @"Can make a pushOperation object.");
    STAssertTrue([self.pushOperation isKindOfClass:[NSOperation class]], @"The pushOperation object is the correct class type.");
}

- (void)testCanInitForTests {
    
    _pushOperation = [[TCSPushOperation alloc] initWithTestFor:@[@"testString", @1234]];
    STAssertNotNil(self.pushOperation, @"Can make a pushOperation with a testArray payload");
    STAssertTrue([self.pushOperation isKindOfClass:[NSOperation class]], @"The pushOperation object is the correct class type.");
}

- (void)testCanSetisFinsihedtoTrue {
    [self.pushOperation setIsFinished:YES];
    STAssertTrue([self.pushOperation isFinished], @"Can set isFinished to YES");
}

- (void)testCanSetisFinishedtoFalse {
    [self.pushOperation setIsFinished:NO];
    STAssertFalse([self.pushOperation isFinished], @"Can set isFinished to NO");
}

@end
