//
//  ViewController.m
//  Push Diagnostics iOS
//
//  Created by Tom Nook on 8/21/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
