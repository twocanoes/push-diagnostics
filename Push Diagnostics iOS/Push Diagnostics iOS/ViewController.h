//
//  ViewController.h
//  Push Diagnostics iOS
//
//  Created by Tom Nook on 8/21/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@end
