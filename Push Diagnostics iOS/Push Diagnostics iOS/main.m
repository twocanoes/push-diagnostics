//
//  main.m
//  Push Diagnostics iOS
//
//  Created by Tom Nook on 8/21/13.
//  Copyright (c) 2013 Twocanoes Software. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
